Usability Tools Description
---------------------------

This module integrates [usability tools] (http://www.usabilitytools.com) widget
with drupal websites. Usability Tools helps tracking user activities on your site.
Once a site is integrated with this tool, it starts recording user activities
on the site and saves a copy in your account on http://usabilitytools.com. By
looking at the recordings one can anylyze what users liked / disliked about his
site.

Features
--------

- Record user activity based on drupal role.
- Record user activity for all roles and exclude few roles.
- Record user activity only on certain urls.
- Record user activity on all urls and exclude few urls.

Get API Key from http://www.usabilitytools.com
----------------------------------------------

An API Key is required for this module to work. One can start by creating a trial
account on http://usabilitytools.com which is valid for around 15 days. Login with
your Google account or sign up. Once logged in click "Install Usability Tools" and
copy the value of projectId (Alphanumeric string in single quotes within the popup)
Save it somewhere as this is the API Key needed on the module's configuration page.
Create a project in order to start recordings of user activities on your site.

Configuration
-------------

  - Once you have enabled this module go to configuration page at
    admin/config/services/usabilitytools.

  - Get your API key from [www.usabilitytools.com] (http://www.usabilitytools.com)
    and put this under "Usability Tools API Key" textfield.

  - Select pages / roles you would like to enable the widget for and you are done.

Track user activities on http://usabilitytools.com
--------------------------------------------------

Once you have provided a valid key it would start recording user activities on
your drupal site and save a recording in your account on http://usabilitytools.com.
