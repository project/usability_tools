/**
 * @file
 * Add widget for usabilitytools.
 */

(function ($, Drupal, document) {
  'use strict';

  Drupal.behaviors.usabilitytools = {
    attach: function (context, settings) {
      window['UsabilityTools Session Recording'] = {
        projectId: settings.usabilityTools.API
      };
      var s = document.createElement('script');
      s.async = true;
      s.src = 'https://mobile.usabilitytools.com/recorder/tracker/tracker.js';
      document.head.appendChild(s);
    }
  };
})(jQuery, Drupal, document);
